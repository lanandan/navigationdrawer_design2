package com.example.material2.model;

public class NavDrawerItem2 {
    private boolean showNotify;
    private String title;


    public NavDrawerItem2() {

    }

    public NavDrawerItem2(boolean showNotify, String title) {
        this.showNotify = showNotify;
        this.title = title;
    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

