package com.example.material2.adapter;

import java.util.Collections;
import java.util.List;

import com.example.material2.R;
import com.example.material2.adapter.NavDrawer2.MyViewHolder;
import com.example.material2.model.NavDrawerItem2;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class NavDrawer2 extends RecyclerView.Adapter<NavDrawer2.MyViewHolder>{

	List<NavDrawerItem2> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    int id;

    public NavDrawer2(Context context, List<NavDrawerItem2> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem2 current = data.get(position);
        holder.title.setText(current.getTitle());
        id = context.getResources().getIdentifier("com.example.material2:drawable/" + "g"+(position+1), null, null);
        holder.image.setImageResource(id);
    }

    @Override 
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            image = (ImageView) itemView.findViewById(R.id.img);
        }
    }
	
}
